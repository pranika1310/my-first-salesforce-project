({
    getCase : function(component) {
        //console.log('into init');
        var action = component.get("c.getCaseDetails");
        var caseID = component.get("v.CaseID");
        //console.log('Rashmi.........caseID '+caseID);
        var _this = this;
        action.setParams({
            "caseId" : caseID          
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                var temp = a.getReturnValue();
                console.log("case details");
                if(typeof temp.Supported_Product_Version__c !=="undefined" ) {
                    _this.showRetText(component,temp.Supported_Product_Version__c);
                }
                if(temp.Bronze_Entitlement__c && temp.TAC_Assigned__c) {
                    $("#bronze-msg").css("display","block");
                }
                if(typeof temp.Customer_Name__c !== "undefined") {
                    $("#customerName").css("display","block");
                }
                
                var desc = temp.Description;
                console.log('desc===',desc);
                var length = desc.length;
                console.log('length====',length);
                if(length<=250){
                    component.set("v.length",true)
                }else if(length>250){
                    component.set("v.length",false);
                    var sub = desc.substring(0,250);
                    component.set("v.desc",sub);
                    console.log("sub===",sub);
                    
                }
                /*  if(temp.CreatedDate != "undefined"){
                    var createdD = new Date(temp.CreatedDate);
                    //console.log('Timezoneoffset..'+createdD.getTimezoneOffset());
                  //  var temp = ;
                	//console.log('temp...'+temp);
                    var dateVar = new Date(createdD.getFullYear(),createdD.getMonth(),createdD.getDate(),createdD.getHours(),createdD.getMinutes(),createdD.getSeconds());
                   // console.log('dateVar...'+dateVar.DateGMT());
                    var localD = dateVar.toLocaleDateString();
                    var localT = dateVar.toLocaleTimeString();
                    console.log('localDT...'+localD + localT);
                    var finalDT = localD + ' '+ localT;
                    console.log('finalDT...'+finalDT);
                    component.set("v.CreatedDate",finalDT);
                }*/
                component.set("v.curCase",temp);
            } else {                
                console.log("error "+a.getError());
                $("#ret-msg").css("display","none");
            }
        });
        $A.enqueueAction(action);
    },
    
    loadStatus : function(component,caseId) {
        var action = component.get("c.getCaseStatus");
        var LockDaysTemp = $A.get("$Label.c.communityLockDays");
        var LockDays = Number(LockDaysTemp);
        action.setParams({
            "caseId" : caseId
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                console.log('Ret Val'+a.getReturnValue());
                var c =a.getReturnValue();
                var obj=JSON.parse(c);
                console.log('has been locked?'+obj.Locked_Case__c);
                component.set("v.caseStatus",obj.Status);
                //component.set("v.LockedCase", obj.Locked_Case__c);
                console.log('days closed '+obj.Days__c);
                if(obj.Days__c < LockDays ) {
                    //$(".reopenbtn").css("display","block");
                    component.set("v.LockedCase",false);
                }
                if(obj.Status == "Closed") {
                    if(obj.Days__c < LockDays) {
                        $("#reopen-container").css("display","block");
                    } else {
                        $("#reopen-container").css("display","none");
                    }
                } else {
                    $("#reopen-container").css("display","none");
                }
                
                
            } 
            else if (a.getState() ==="ERROR") {
                console.log('has error');
                console.log(a.getError());
            }         
        });
        $A.enqueueAction(action);
    },
    
    showRetText : function(component,spvId) {
        var action = component.get('c.generateRetirementBanner');
        action.setParams({
            "spvId":spvId 
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                var message = a.getReturnValue();
                if(message != '') {
                    component.set("v.retirementText",message);
                    $("#ret-msg").css("display","block");
                } else {
                    $("#ret-msg").css("display","none");   
                }
                
            } else {
                console.log('error '+a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setSurveyBox: function(component, event, helper) {
        component.set("v.surveyBox", true);
        window.location.reload()
    },    
    
    goToSurveyURL: function (component, event, helper) {
        var url="https://www.getfeedback.com/r/RUu79kGu?case__c="+component.get("v.curCase.Id")+"&contact__c="+component.get("v.curCase.Contact.Id")+"&account__c="+component.get("v.curCase.Account.Id")+"&Portal_Survey__c=ThankYouForYourFeedback";
        // var url="https://www.getfeedback.com/r/RUu79kGu?case__c="+component.get("v.curCase.Id")+"&contact__c="+component.get("v.curCase.Contact.Id")+"&account__c="+component.get("v.curCase.Account.Id");        
        //    var url="http://survey.clicktools.com/app/survey/go.jsp?iv="+$A.get("$Label.c.CSATSurveyUniqueId")+"&q2="+component.get("v.curCase.Id")+"&q4="+component.get("v.curCase.Contact.Id")+"&q5="+component.get("v.curCase.Account.Id")+"&q14="+$A.get("$Label.c.CSATSurveyRecordTypeId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    updateSurveyVrbls: function (component) {
        var caseID = component.get("v.CaseID");
        var curCase =component.get("v.curCase");
        var action = component.get("c.updateSurveyVrbls");
        action.setParams({
            "caseId" : caseID,
            "curCase" : curCase
        });
        action.setCallback(this,function(a) {
            if(a.getState() === "SUCCESS") {
                console.log(a.getReturnValue());
            } else {
                console.log("ERROR "+a.getError());
            }    
        });
        $A.enqueueAction(action);
        
    },
    
    loadReasonPicklist:function(component) 
    {
        var action = component.get("c.getReasonPicklist");
        
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                console.log('Ret Val'+a.getReturnValue());
                var opts=a.getReturnValue();
                console.log('Ret Valarr'+opts);
                arrStatus=opts.split(",");
                
                console.log('arrStatus'+arrStatus[0]);
                component.set("v.optionss", arrStatus);
                // find("InputSelectDynamic")
            } 
            else if (a.getState() ==="ERROR") {
                console.log('has error');
                console.log(a.getError());
            }         
        });
        $A.enqueueAction(action);
    },
    
    setReopenStatus:function(component,caseId,optionss,otherError)
    {
        var action = component.get("c.reopenCase");
        action.setParams({
            "caseId" : caseId,
            "optionss" : optionss,
            "otherError": otherError
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                console.log('success or not?'+a.getReturnValue());
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": caseId,
                    "slideDevName": "related"
                });
                navEvt.fire();    
            } else if (a.getState() ==="ERROR") {
                console.log('has error');
                console.log(a);
            }         
        });
        $A.enqueueAction(action);		
    },
    
    createNewComment : function(component,caseId,commentBody) {
        
        console.log('anupatil in helper of createNewcomment flow 3');
        var action = component.get("c.insertCaseComment");
        var curCase = component.get("v.curCase");
        action.setParams({
            "caseId" : caseId,
            "commentBody" : commentBody
        });
        
        action.setCallback(this,function(a){
            
            if(a.getState() === "SUCCESS") {
                console.log('anupatil comment created return of helper flow 4');
                console.log("Comment inserted ? " + a.getReturnValue());
                var isSuccess = a.getReturnValue();
                var file = document.getElementById('fileNew');  
                var fileArray1=[];
                if(file !=undefined){
                    fileArray1 = file.files;
                }
                if(isSuccess) {
                    console.log('anupatil now refresh the page flow 5');
                    console.log('Is case closed? '+curCase.IsClosed);
                    if(!curCase.IsClosed && fileArray1.length>0) {
                        console.log('anupatil before refreshing view');
                        console.log('anupatil after refreshing view');
                    }else if(!curCase.IsClosed && fileArray1.length==0){
                        window.location.reload();
                        
                    }
                    
                    
                }
            } else if (a.getState() ==="ERROR") {                
                console.log('has error in createNewComment Helper communityCreateComment');
                console.log(a);
            }         
        });
        $A.enqueueAction(action);					
    },
    
    initializeSDK : function (component) {
        var action = component.get("c.getS3Params");
        action.setCallback(this,function(a) {
            if(a.getState() === "SUCCESS") {
                var res = a.getReturnValue();
                // console.log('res:::::'+res);
                var JSONRES = JSON.parse(res);
                // console.log('JSONRES host:::::::'+JSONRES.host);
                component.set("v.s3Props",JSONRES);
                //AWS.config.update({accessKeyId: JSONRES.accessKey, secretAccessKey: JSONRES.secretKey});                
                //AWS.config.region = JSONRES.host;
            }  
        });
        $A.enqueueAction(action);
    },
    
    //added by grazitti to upload files from case detail
    createAttachments : function(component,bucketname,filename,caseNumber,fileType,fileSize,fileFinal,isLastFile) {
        //console.log('in create Attachment function');
        var action = component.get("c.insertAttach");
        var caseId = component.get("v.CaseID");
        
        console.log(caseId+' '+filename+' '+bucketname+' '+caseNumber+' '+fileType+' '+fileSize+' '+fileFinal);
        //console.log("filesize:"+filesize);
        
        action.setParams({
            "caseId" : caseId,
            "filename" : filename,
            "bucket" : bucketname,
            "caseNumber" : caseNumber,
            "fileType" : fileType,
            "fileSize" : fileSize,
            "fileFinal" : fileFinal
        });
        console.log('action===',JSON.stringify(action));
        action.setCallback(this,function(a){
            //console.log(a.getState());
            //console.log(a.getReturnValue());
            if(a.getState() ==="SUCCESS") {
                var message=a.getReturnValue();                
                if(message.indexOf('success')==0) {
                    var action2 = component.get("c.resetStatus");
                    var caseId = component.get("v.CaseID");        
                    action2.setParams({
                        "caseId" : caseId,            		
                        "caseNumber" : caseNumber            		
                    });
                    action2.setCallback(this, function(a){
                        //console.log(a.getState()+'--'+ a.getReturnValue());
                        if(a.getState()==='SUCCESS'){
                            var message=a.getReturnValue();
                            if(message.indexOf('success')==0){                            
                                //console.log('Case status is flipped to open');  
                            }else
                                //console.log('Error in flipping case status to Open OR Case is already open/Closed');
                                // $A.get('e.force:refreshView').fire();
                                if(isLastFile){
                                    window.location.reload();
                                }
                        }
                    });
                    $A.enqueueAction(action2);
                    //changes done by anupatil ends
                } else {
                    console.log('error'+a.getError());
                }
            }                   
        });
        $A.enqueueAction(action);
    },
    getNumber : function(component,caseID) {
        var action = component.get("c.getCaseNumber");
        action.setParams({
            "caseID":caseID
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                //console.log(a.getReturnValue());
                if(a.getReturnValue().length > 0) {
                    component.set("v.caseNumber",a.getReturnValue());
                }
            } else {
                console.log("error in loading attachments "+a.getError());
            }
        });
        $A.enqueueAction(action);	    
    },
    
    //added by grazitti to upload files from case detail
    uploadFiles : function (component,event){
        var s3Settings = component.get("v.s3Props");
        var fileArray =   component.get("v.FilesArray");
        var fileArrayIndex =   component.get("v.FilesArrayIndex");
        var filelist=fileArray[0];
        var file=filelist[fileArrayIndex];
        fileArrayIndex = fileArrayIndex+1;
        var filename =file.name;
        $("#loading-graphic1").css("display","block");
        $("#dl-progress1").css("display","block");
        let button = event.getSource();
    	button.set('v.disabled',true);
        //$(".submitButton").css("display","none");
        var d = new Date();
        var n = d.getTime();
        var find = ','; 
        console.log("file.name...."+ file.name);
        var filenamewithoutdate = replaceAll(filename, find, '');
        console.log("filenamewithoutdate...."+ filenamewithoutdate);
        var filenamewithdate = filenamewithoutdate.substring(0, filenamewithoutdate.lastIndexOf('.'))+n+'.'+filenamewithoutdate.slice(filenamewithoutdate.lastIndexOf('.')+1);
        var filetype = filenamewithoutdate.substring(filenamewithoutdate.lastIndexOf(".") + 1);
        var formatedDate = new Date().toGMTString(); 
        
        /*Incident-14483 To change amazon code from V2  to  V4 Begin */ 
        var contdispparm = "attachment;filename="+"\""+filenamewithoutdate+"\"";
        
        var caseNumber = component.get("v.caseNumber");
        
        var action = component.get("c.uploadToAWSS3Apex");       
        action.setParams({    
            "filename" : filenamewithdate,
            "caseNumber" : caseNumber
        });
        action.setCallback(this,function(a) {
            if(a.getState()==="SUCCESS") {
                var strEndPoint = a.getReturnValue();   
                console.log("Rashmi...."+strEndPoint);
                var xhr = new XMLHttpRequest();          
                
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = Math.round((evt.loaded / evt.total) * 100);
                        console.log("Progress:" + percentComplete);
                        $('#progress').css('width', percentComplete +"%").attr('aria-valuenow', percentComplete);  
                    }
                }, false);
                //progress bar changes end here
                
                xhr.open('PUT', strEndPoint, true);                 
                
                xhr.onload = () => {
                    // alert(xhr.status);
                    if (xhr.status === 200) {
                    console.log('status code here.......',xhr.status);
                    var filesize = '';
                    var bytes = file.size;
                    var decimals = 2;
                    //if(bytes == 0) return '0 Byte';
                    var k = 1000;
                    var dm = decimals + 1 || 3;
                    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                    var i = Math.floor(Math.log(bytes) / Math.log(k));
                    filesize = (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
                    var isLastFile = filelist.length<=fileArrayIndex;
                    this.createAttachments(component,s3Settings.bucket,filenamewithoutdate,caseNumber,filetype,filesize,filenamewithdate,isLastFile);
                    var caseClosed = $A.get("$Label.c.communityCaseCommentMsg");
                    var caseClosedLock = $A.get("$Label.c.communityCaseCommentLockedMsg");
                    var LockDaysTemp = $A.get("$Label.c.communityLockDays");
                    var LockDays = Number(LockDaysTemp);
                    var curCase = component.get("v.curCase"); 
                    
                    if(curCase.IsClosed){
                    if(curCase.Days__c < LockDays) {
                    component.find("prompt-text").set("v.value",caseClosed);
                    $("div.modal-wrapper").css("display","block");    
                }else{
                    if(curCase.Days__c >= LockDays) {
                        component.find("prompt-text").set("v.value",caseClosedLock);    
                    }
                    $("div.modal-wrapper").css("display","block");     
                }
            }                    
            //changes done by anupatil ends here
            var attachList = component.get("v.caseAttachments");
            var newAttach = {"Amazon_S3_location__c":s3Settings.bucket+'/'+caseNumber+'/'+filenamewithdate,"File_Name__c":filenamewithoutdate,"File_Size__c":filesize}
            /*newAttach.Amazon_S3_location__c = s3Settings.bucket+'/'+caseNumber+'/'+filenamewithdate;
                                		newAttach.File_Name__c = file.name;
                                		newAttach.File_Size__c = filesize;*/
            attachList.push(newAttach);
            component.set("v.caseAttachments",attachList);
            if(filelist.length>fileArrayIndex){
                component.set('v.FilesArrayIndex',fileArrayIndex);
                button.set('v.disabled',false);
                this.uploadFiles(component,event);
                
                
            }else{
                $("#loading-graphic1").css("display","none");
                $('#progress').css('width', 0+'%').attr('aria-valuenow', 0); 
                $("#dl-progress1").css("display","none");
               // $(".submitButton").css("display","block");
                 $("#message-grazitti").css("display","none");
                button.set('v.disabled',false);
                
            }
        } 
                           };
                           
                           xhr.onerror = () => {
            // error...
        };
        /*Incident-14483 To change amazon code from V2  to  V4 Begin */ 
        xhr.setRequestHeader("Content-Disposition", contdispparm);
        /*Incident-14483 To change amazon code from V2  to  V4 End */ 
        // Upload the file
        xhr.send(file);
        
        
    }
    else {
    console.error(message);
}	
 });
$A.enqueueAction(action);
},
    
    insertFile:function(component,event){
        
        var fileArray1=[];
        var fileChooser1 = document.getElementById('fileNew');
        if(fileChooser1 !=undefined){
            fileArray1 = fileChooser1.files;
        }
        var restrictedFileTypes = ["PHP","ACTION","APK","APP","BAT","BIN","CMD","COM","COMMAND","CPL","CSH","EXE","GADGET","INF","INF1","INS","INX","IPA","ISU","JOB","JSE","KSH","LNK","MSC","MSI","MSP","MST","OSX","OUT","PAF","PIF","PRG","PS1","REG","RGS","RUN","SCR","SCT","SHB","SHS","U3P","VB","VBE","VBS","VBSCRIPT","WORKFLOW","WS","WSF","WSH"];
        var restrictedFileTypesLC = ["php","action","apk","app","bat","bin","cmd","com","command","cpl","csh","exe","gadget","inf","inf1","ins","inx","ipa","isu","job","jse","ksh","lnk","msc","msi","msp","mst","osx","out","paf","pif","prg","ps1","reg","rgs","run","scr","sct","shb","shs","u3p","vb","vbe","vbs","vbscript","workflow","ws","wsf","wsh"];
        
        if (fileArray1 && fileArray1.length>0){ 
            for(var count1=0;count1<fileArray1.length;count1++){
                var filename1 = fileArray1[count1].name;
                var filetype1 = filename1.substring(filename1.lastIndexOf(".") + 1);
                var size1 = fileArray1[count1].size;
                
                //code added as a part of Incident 18360 
                var fileTypeLC = filetype1.toLowerCase();
                for(var i=0; i<restrictedFileTypes.length; i++){
                    if(fileTypeLC.includes(restrictedFileTypes[i]) || fileTypeLC.includes(restrictedFileTypesLC[i])){
                        $("#restrictedFileTypesErrorBox1").css("display","block");
                        $("#message-grazitti").css("display","none");
                        return;
                    }
                }             
                if(size1 > ($A.get("$Label.c.communityS3MPFileSizeLimit"))){
                    $("#largerFilesErrorBox1").css("display","block");
                    $("#message-grazitti").css("display","none");
                    return;
                }
            }
            component.set('v.FilesArrayIndex',0);
            component.set('v.FilesArray',fileArray1);
            
            this.uploadFiles(component,event);
        }
        
    },
        
        insertComment:function(component,event){
            console.log('Anupatil in insertComment function flow 1');
            var commentBody = component.get("v.commentBody");
            if(commentBody !=undefined){
                var commentLength = commentBody.length;  
            }
            var caseId = component.get("v.CaseID");
            var curCase = component.get("v.curCase");
            var fileArray1=[];
        	var fileChooser1 = document.getElementById('fileNew');
        	if(fileChooser1 !=undefined){
            	fileArray1 = fileChooser1.files;
        }
            
            if(commentBody != "" && typeof commentBody !="undefined" ) {  
                if(commentLength <= 3900){
                    if(fileArray1.length==0){
                        $("div.modal-submit-wrapper").css("display","block"); 
                    }else{
                        $("#message-grazitti").css("display","block");
                        
                    }
                    
                    
                    console.log('anupatil calling helper createNewComment flow 2');
                    this.createNewComment(component,caseId,commentBody);
                    if(curCase.IsClosed) {
                        $("div.modal-submit-wrapper").css("display","none");
                        if(curCase.Days__c < LockDays) {
                            component.set("v.promptText",caseClosed);
                            $("div.modal-wrapper").css("display","block");    
                        } else {
                            if(curCase.Days__c >= LockDays) {
                                component.set("v.promptText",caseClosedLock);    
                            }
                            $("div.modal-wrapper-grazitti").css("display","block");     
                        }
                    }
                }
                else{
                    $("div.modal-wrapper-grazitti").css("display","block"); 
                    component.set("v.promptText","Kindly note that Case Comments are limited to 3900 characters. Your entry is " + commentLength +" characters. Please re-enter your comment.");
                    return;
                }
            }
            
            
            
            
            
            
        },
            
            
            
})