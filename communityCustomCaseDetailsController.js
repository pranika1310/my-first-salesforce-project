({
    doInit : function(component, event, helper) {
        console.log("into Case detail....");
        var caseId = component.get("v.CaseID");
        helper.getCase(component);
        helper.loadStatus(component,caseId);
        helper.loadReasonPicklist(component);
        helper.initializeSDK(component);
        helper.getNumber(component,caseId);
        //  helper.getUserPerm (component);
        //helper.getCase2(component);
    },
    
    
    
    handleNotifySaveAddress : function(component, event, helper) {
        var succMessage = event.getParam("succMessage");
        if(succMessage){
            console.log('Success message ...');
            // component.set("v.successMsg", 'Email CC list has saved successfully.');
            helper.getCase(component);
            //var EmailCCList = event.getParam("EmailCCList");
            //console.log('EmailCCList..'+EmailCCList);
            //component.set("v.curCase.Email_CC_List__c",'Help');
        }
    },
    
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        console.log('Close function');
        
        component.set("v.curCase.Resolution__c",'');
        // for Hide/Close Model,set the "isOpen" attribute to "False"  
        component.set("v.isOpen", false);
        component.set("v.surveyBox", false);
    },
    
    closeCase: function(component, event, helper) {
        var action = component.get("c.getCaseClosed");
        var caseID = component.get("v.CaseID");
        var curCase =component.get("v.curCase");
        console.log('caseID.........caseID '+curCase.Reason);
        console.log('caseID.........caseID '+curCase.Sub_Reason_pl__c);
        console.log('caseID.........caseID '+curCase.Found_Answer_In__c);
        var _this = this;
        action.setParams({
            "caseId" : caseID ,
            "curCase" : curCase
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                var temp = a.getReturnValue();
                console.log('getCaseClosed return value..'+temp);
                component.set("v.curCase.Customer_Status_Formula__c", 'Closed');
                component.set("v.curCase.Resolution__c", temp);
                
                helper.setSurveyBox(component, event, helper);
                
                
            } else {                
                var errors = a.getError();
                console.log("error "+errors[0].pageErrors[0].message);
                // component.set("v.errorMsg", errors[0].pageErrors[0].message);
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    caseSurvey: function(component, event, helper) {
        var action = component.get("c.getCaseDetails");
        var caseID = component.get("v.CaseID");
        var curCase =component.get("v.curCase");
        
        var _this = this;
        action.setParams({
            "caseId" : caseID,
            "curCase" : curCase
        });
        action.setCallback(this,function(a){
            if(a.getState() === "SUCCESS") {
                var temp = a.getReturnValue();
                if(((component.get("v.curCase.Account.Name") != "Customer Support Holding Account") || (component.get("v.curCase.Account.Name") != "TIBCO") || (component.get("v.curCase.Account.Name") != "Eval") || (component.get("v.curCase.Account.Name") != "Jaspersoft") || (component.get("v.curCase.Account.Name") != "Beta")) && (component.get("v.curCase.Primary_Queue__c") != "Contacts/Web User Id") && ((component.get("v.curCase.Account.Do_Not_Survey__c") != true) || (component.get("v.curCase.Do_Not_Survey__c") != true)) && (((component.get("v.curCase.Contact.Email")).includes("@tibco.com")) != true)){ 
                    helper.goToSurveyURL(component,event,helper);
                    component.set("v.surveyBox", false);
                    component.set("v.curCase.Survey_Sent__c", true);
                    component.set("v.curCase.Survey_Last_Sent_Date__c", new Date());
                    helper.updateSurveyVrbls(component);
                }	
            } else {                
                var errors = a.getError();
                console.log("error "+errors[0].pageErrors[0].message);
                // component.set("v.errorMsg", errors[0].pageErrors[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    
    goToCaseListURL: function (component, event, helper) {
        var url = $A.get("$Label.c.communityBaseUrl")+'mycases';
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('url...'+url);
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    
    
    reOpenCase1:function(component,event,helper){
        component.set("v.openCase",true);
        
    },
    
    reOpenCase : function(component, event, helper) {
        
        console.log('extraaa');
        
        var caseId = component.get("v.CaseID");
        var optionss = component.get("v.reason");
        var otherError=component.get("v.otherError");
        
        console.log('test---'+otherError); 
        if(optionss === "None" || (typeof optionss === 'undefined')){
            console.log('extraaa options---> +++' +optionss);
            component.set("v.errorLabel",'Please select a reason for reopening');
        }
        else{
            $("div.modal-reopen-wrapper").css("display","block");
            helper.setReopenStatus(component,caseId,optionss,otherError); 
            
        }
        
    },
    
    
    
    closeModel1:function(component,event,helper){
        component.set("v.openCase",false);
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    showSpinner1: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner1", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner1 : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner1", false);
    },
    
    openDescModel:function(component,event,helper){
        component.set("v.showDescription",true);
    },
    
    closeModel11:function(component,event,helper){
        component.set("v.showDescription",false);
    },
    
    submitButtonClick:function(component,event,helper){
       
        var commentBody = component.get("v.commentBody");
        var fileChooser1 = document.getElementById('fileNew');
        console.log(component.get("v.CaseID"));
        var fileArray1=[];
        fileArray1 = fileChooser1.files;
        if(commentBody !=undefined){
           helper.insertComment(component,event);
        }
        if(fileArray1.length!=0){
            helper.insertFile(component,event);
        }
    },
    
    
    
    
    //changes added by grazitti
    
    
    closeModalError : function(component,event) {
        $("#restrictedFileTypesErrorBox1").css("display","none");
        var curCase = component.get("v.curCase");
        
        
    },
    
    updateCharCount : function(component, event){
          //console.log('Rashmi......inside function');  
          var commentBody = component.get("v.commentBody");
          var commentLength = commentBody.length;
          //console.log('Rashmi.....' + commentBody);
          var cr = 3900 - commentLength;
          //console.log('Rashmi.....' + cr);
          if(cr >= 0){
             //$('#characters-remaining').css({"font-color":"black"}); 
             $('#characters-remaining').text(cr + ' characters remaining').css("color", "black");
              
          }
          else{
             //$('#characters-remaining').css({"font-color":"red"});  
             $('#characters-remaining').text((commentLength - 3900) + ' characters over').css("color", "red"); 
          }
        
    },
    
    closeModal:function(component,event){
          $("div.modal-wrapper-grazitti").css("display","none"); 

        
    }
    
 
 })